FROM bufbuild/buf AS buf
FROM dart:stable AS dart

COPY --from=buf /usr/local/bin/buf /usr/local/bin/

WORKDIR /dart-sass
RUN git clone https://github.com/sass/dart-sass.git . && \
    git checkout 1.77.1 && \
    dart pub get && \
    dart run grinder protobuf

RUN dart compile exe bin/sass.dart -o /usr/local/bin/sass

FROM debian:bookworm-slim

COPY --from=dart /usr/local/bin/sass /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/sass"]
