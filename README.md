# sass-docker

The source for my `dart-sass` Docker image. See: https://hub.docker.com/r/willywongi/dart-sass. Available for `linux/amd64` and `linux/arm64` platforms.
